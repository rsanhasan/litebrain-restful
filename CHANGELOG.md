# Changelog

All notable changes to this project will be documented in this file.
## [Development]

## 2020-10-5

### Added

- README.md telah tersedia.
- LICENSE telah ditambahkan, dengan menggunakan lisensi MIT.
- Requirement.txt file untuk daftar library yang digunakan.
- CHANGELOG.md telah tersedia untuk mencatat daftar perubahan.
