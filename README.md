# LiteBrain RESTful API
REST API dengan Python dan Framework Flask untuk device local seperti raspbery pi .

## Instal dan Penggunaan
* Download Project ini, lalu letakan pada direktori home
* Buat virtual environment dengan `virtualenv --python3 venv` lalu `source venv/bin/activate`
* Instal requirements dengan `pip install -r requirements.txt`
* Inisiasi database sqlite dengan `python3 manage.py db init`
* lalu `python3 manage.py db migrate` dan `python3 manage.py db upgrade`
* Jalankan service dengan `flask run`

## Getting Started

## Allowed HTTP methods 

## 1. GET

## 2. POST

## 3. PUT

## 4. DELETE

