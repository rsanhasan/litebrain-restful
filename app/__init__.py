from flask import Flask
from flask_sqlalchemy import  SQLAlchemy
from flask_marshmallow import Marshmallow
from config import DevelopmentConfig
# import os

# Init App
app = Flask(__name__)
app.config.from_object(DevelopmentConfig)
SECRET_KEY = app.config['SECRET_KEY']

# Init Database
db = SQLAlchemy(app)
Column = db.Column

# Init Marshmallow
ma = Marshmallow(app)

# Run Server
from app import models, routes