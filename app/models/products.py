from app import app, db, ma, Column

# Models
class Product(db.Model):
    id = Column(db.Integer, primary_key=True)
    name = Column(db.String(100))
    description = Column(db.String(200))
    price = Column(db.Float)
    quantity = Column(db.Integer)

    def __init__(self, name, description, price, quantity):
        self.name = name
        self.description = description
        self.price = price
        self.quantity = quantity

#Schema
class Schema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'description', 'price', 'quantity')

# Init Schema
product_schema = Schema()
products_schema = Schema(many=True) 