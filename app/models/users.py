from app import app, db, ma, Column

class User(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(32), index = True)
    password_hash = db.Column(db.String(128))

    def __init__(self, username, password_hash):
        self.username = username
        self.password_hash = password_hash

#Schema
class Schema(ma.Schema):
    class Meta:
        fields = ('id', 'username', 'password_hash')

# Init Schema
user_schema = Schema()
users_schema = Schema(many=True) 