from app import app, db
from flask import request, jsonify
from app.models.products import Product, product_schema, products_schema
from app.routes.authentication import token_required

# Create Product
@app.route('/product', methods=['POST'])
def add_product():
    name = request.json['name']
    description = request.json['description']
    price = request.json['price']
    quantity = request.json['quantity']

    product = Product(name, description, price, quantity)

    db.session.add(product)
    db.session.commit()

    return product_schema.jsonify(product) 

# Get All Products
@app.route('/product', methods=['GET'])
@token_required
def get_products(current_user):
    all_products = Product.query.all()
    results = products_schema.dump(all_products)

    return jsonify(results)

# Get Single Product
@app.route('/product/<id>', methods=['GET'])
def get_product(id):
    product = Product.query.get(id)
    result = product_schema.dump(product)

    return jsonify(result)

# Update a Product
@app.route('/product/<id>', methods=['PUT'])
def update_product(id):
    product = Product.query.get(id)

    name = request.json['name']
    description = request.json['description']
    price = request.json['price']
    quantity = request.json['quantity']

    product.name = name
    product.description = description
    product.price = price
    product.quantity = quantity

    db.session.commit()

    result = product_schema.dump(product)

    return jsonify(result)

# Get Single Product
@app.route('/product/<id>', methods=['DELETE'])
def unlink_product(id):
    product = Product.query.get(id)
    db.session.delete(product)
    db.session.commit()
    result = product_schema.dump(product)

    return jsonify(result)

