import logging
import jwt
from datetime import datetime, timedelta
from app import app, db, SECRET_KEY
from flask import request, jsonify, abort
from werkzeug.security import generate_password_hash, check_password_hash
from app.models.users import User, user_schema, users_schema
from app.routes.authentication import token_required

# Create Product
@app.route('/api/users', methods=['POST'])
def register_user():
    username = request.json['username']
    password = request.json['password']
    if username is None or password is None: return jsonify({'message': 'Username or Password is invalid!'}), 400
    
    user = User.query.filter_by(username=username).first()
    if user is not None: return jsonify({'message': 'Username is invalid!'}), 400
    
    hashed_password = generate_password_hash(password, method='sha256')
    user = User(username, hashed_password)

    try:
        db.session.add(user)
        db.session.commit()
        app.logger.info('Register new user')
        return user_schema.jsonify(user)
    except:
        return jsonify({"error": "Error session add data"}), 400

# Login User
@app.route('/api/login', methods=['GET'])
def login_user():
    auth = request.authorization
    if not auth or not auth.username or not auth.password: return jsonify({'message': 'Username or Password is invalid!'}), 401

    user = User.query.filter_by(username=auth.username).first()
    if not user: return jsonify({'message': 'Username is invalid!'}), 401

    if not check_password_hash(user.password_hash, auth.password): return jsonify({'message': 'Password is invalid!'}), 401

    expired = datetime.utcnow() + timedelta(minutes=15)
    token = jwt.encode({'username': user.username, 'exp': expired}, SECRET_KEY)
    return jsonify({'token': token.decode('UTF-8')})
        


# Get Multi Users
@app.route('/api/users', methods=['GET'])
@token_required
def get_users(current_user):
    users = User.query.all()
    results = users_schema.dump(users)

    return jsonify(results)

# Get One User
@app.route('/api/users/<id>', methods=['GET'])
@token_required
def get_user(current_user, id):
    user = User.query.get(id)
    result = user_schema.dump(user)

    return jsonify(result)
